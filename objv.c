#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <arpa/inet.h>
#include <float.h>
#include <stdio.h>

#include "geom.h"
#include "parser.h"
#include "util.h"


#define N_VBO 2
#define N_VAO 1

enum {LocAttrib, LocUniform}; /* Shader location type */
enum {ProgBase, ProgText, NbProgs}; /* Shader prog type */


typedef struct {
	char *title;
	int w;
	int h;
	int x;
	int y;
} Window;

typedef struct {
	char *file;
	GLenum type;
} Shader;

typedef struct {
	unsigned int c;
	float pos[3];
	float color[4];
} Glyph;

// typedef struct {
// 	GLfloat v[4];
// 	GLfloat vt[3];
// 	GLfloat vn[3];
// } Vertex;
typedef struct {
	int animating;
	float animangle;
	int hasnormals;
	int wireframe;
	int revnormals;
} State;


/* declarations */
static void loadshaders(GLuint progs[], const Shader *shaders, int len);
static GLuint loadtexture(const char *fname, GLenum texunit);
static GLchar *readfile(const char *file);
static GLuint getloc(GLuint prog, const GLchar *name, const int type);
static void loadlocs();
static void init();
static void animate();
static void run();
static int events();
static void keyboard(SDL_Keycode key);
static void quit();


/* variables */
static Window win = {"objv", 800, 600, 100, 100};
static SDL_Window *sdlwin;
static SDL_GLContext *glcontext;
static Mat4 matrModel = {{0}};
static Mat4 matrVisu = {{0}};
static Mat4 matrProj = {{0}};
static Mat4 matrNorm = {{0}};
static GLuint progs[NbProgs];
static GLuint vao[N_VAO];
static GLuint vbo[N_VBO];

static GLuint locmatrmodel;
static GLuint locmatrvisu;
static GLuint locmatrproj;
static GLuint locmatrnorm;
static GLuint locvertex;
static GLuint loccolor;
static GLuint locchar;
static GLuint loctexcoord;
static GLuint loctex;
static GLuint locnormal;
static GLuint lochasnormals;
static GLuint locrevnormals;

static GLuint texture;

static const Uint32 sdlflags = SDL_INIT_VIDEO | SDL_INIT_EVENTS;
static const Shader textshaders[] = { /* shader prog must start with vertex shader */
	{"text.vert",   GL_VERTEX_SHADER},
	{"text.geom",   GL_GEOMETRY_SHADER},
	{"text.frag", GL_FRAGMENT_SHADER},
	// {"textvert.glsl", GL_VERTEX_SHADER},
	// {"textgeom.glsl", GL_GEOMETRY_SHADER},
	// {"textfrag.glsl", GL_FRAGMENT_SHADER}
};
static const Shader shaders[] = { /* shader prog must start with vertex shader */
	{"simple.vert",   GL_VERTEX_SHADER},
	{"simple.geom",   GL_GEOMETRY_SHADER},
	{"simple.frag", GL_FRAGMENT_SHADER},
};
static const unsigned char hellotext[] = "Hello World!";
static const GLfloat cube[3*4*6] = {
	-1.0,  1.0, -1.0,    1.0,  1.0, -1.0,  -1.0, -1.0, -1.0,    1.0, -1.0, -1.0,   // P3,P2,P0,P1
	1.0, -1.0,  1.0,   -1.0, -1.0,  1.0,   1.0, -1.0, -1.0,   -1.0, -1.0, -1.0,   // P5,P4,P1,P0
	1.0,  1.0,  1.0,    1.0, -1.0,  1.0,   1.0,  1.0, -1.0,    1.0, -1.0, -1.0,   // P6,P5,P2,P1
	-1.0,  1.0,  1.0,    1.0,  1.0,  1.0,  -1.0,  1.0, -1.0,    1.0,  1.0, -1.0,   // P7,P6,P3,P2
	-1.0, -1.0,  1.0,   -1.0,  1.0,  1.0,  -1.0, -1.0, -1.0,   -1.0,  1.0, -1.0,   // P4,P7,P0,P3
	-1.0, -1.0,  1.0,    1.0, -1.0,  1.0,  -1.0,  1.0,  1.0,    1.0,  1.0,  1.0    // P4,P5,P7,P6
};
static const GLfloat cubetex[2*4*6] = {
	1., 1.,  0., 1.,  1., 0.,  0., 0.,
	1., 1.,  0., 1.,  1., 0.,  0., 0.,
	1., 0.,  1., 1.,  0., 0.,  0., 1.,
	1., 1.,  0., 1.,  1., 0.,  0., 0.,
	1., 0.,  1., 1.,  0., 0.,  0., 1.,
	1., 1.,  0., 1.,  1., 0.,  0., 0.
};
static State state = {
	.animating=1,
	.animangle=0.0,
	.hasnormals=0,
	.wireframe=0,
	.revnormals=0,
};
static AABB bbox = { // Bounding box of the obj, ready to find min/max
	{FLT_MAX, FLT_MAX, FLT_MAX},
	{-FLT_MAX, -FLT_MAX, -FLT_MAX}
};


static Vertex *vertices = NULL;
static GLuint *indices = NULL;
static size_t lenvtx = 0;
static size_t lenidx = 0;


/* definitions */
static void
loadshaders(GLuint progs[], const Shader *shaders, int len)
{
	int i;
	int j;
	GLchar *shader;
	GLuint shaderobj;

	for (i = 0; i < NbProgs; i++) {
		progs[i] = glCreateProgram();
		for (j = 0; j < len; ++j) {
			shader = readfile(shaders[j].file);
			shaderobj = glCreateShader(shaders[j].type);
			glShaderSource(shaderobj, 1, (const GLchar **)(&shader), NULL);
			glCompileShader(shaderobj);
			glAttachShader(progs[i], shaderobj);
			printcomplog(shaderobj);
			free(shader);
		}
		glLinkProgram(progs[i]);
		printprogramlog(progs[i]);
	}
}

static GLuint
getloc(GLuint prog, const GLchar *name, const int type)
{
	GLuint loc;
	switch (type) {
	case LocAttrib:
		loc = glGetAttribLocation(prog, name);
		break;
	case LocUniform:
		loc = glGetUniformLocation(prog, name);
		break;
	}
	if (loc == -1)
		fprintf(stderr, "Cannot find location for %s\n", name);
	return loc;
}

static void
loadlocs()
{
	loccolor = getloc(progs[ProgBase], "Color", LocAttrib);
	//locvertex = getloc(progs[ProgBase], "Vertex", LocAttrib);
	locvertex = getloc(progs[ProgBase], "v", LocAttrib);
	//loctexcoord = getloc(progs[ProgBase], "TexCoord", LocAttrib);
	loctexcoord = getloc(progs[ProgBase], "vt", LocAttrib);
	locnormal = getloc(progs[ProgBase], "vn", LocAttrib);
	loctex = getloc(progs[ProgBase], "tex", LocUniform);
	locchar = getloc(progs[ProgBase], "glyph", LocAttrib);
	lochasnormals = getloc(progs[ProgBase], "hasnormals", LocUniform);
	locrevnormals = getloc(progs[ProgBase], "revnormals", LocUniform);
	locmatrmodel = getloc(progs[ProgBase], "matrModel", LocUniform);
	locmatrvisu = getloc(progs[ProgBase], "matrVisu", LocUniform);
	locmatrproj = getloc(progs[ProgBase], "matrProj", LocUniform);
	locmatrnorm = getloc(progs[ProgBase], "matrNorm", LocUniform);
}

static GLchar *
readfile(const char *file)
{
	FILE *f;
	long flen;
	GLchar *str;

	if (!(f = fopen(file, "r")))
		die("%s could not be read\n", file);

	fseek(f, 0, SEEK_END);
	flen = ftell(f); /* get file length */
	rewind(f);
	str = calloc(flen + 2, sizeof(char));
	if (!str)
		die("Insufficient memory to load %s\n", file);

	fread(str, flen, sizeof(char), f);
	fclose(f);
	str[flen + 1] = '\0';
	return str;
}

static GLuint
loadtexture(const char *fname, GLenum texunit)
{
	GLuint t;
	FILE *f;
	uint64_t rowlen;
	uint32_t hdr[4], w, h, i, j;
	uint16_t *row;
	char *pixels;

	if (!(f = fopen(fname, "r")))
		die("%s could not be read\n", fname);

	if (fread(hdr, sizeof(uint32_t), 4, f) != 4)
		die("%s: fread error\n", fname);

	if (memcmp("farbfeld", hdr, sizeof("farbfeld") - 1))
		die("%s: invalid farbfeld magic value\n", fname);
	w = ntohl(hdr[2]);
	h = ntohl(hdr[3]);
	pixels = (char *)ecalloc(w * h * (sizeof("RGBA") - 1), sizeof(char));

	rowlen = (sizeof("RGBA") - 1) * w;
	row = ecalloc(rowlen, sizeof(uint16_t));

	for (i = 0; i < h; i++)
	{
		if (fread(row, sizeof(uint16_t), rowlen, f) != rowlen)
			die("%s: fread error\n", fname);
		for (j = 0; j < rowlen; j++)
			pixels[(h-i-1)*rowlen+j] = row[j] & 0xFF;
	}

	glGenTextures(1, &t);
	glActiveTexture(texunit);
	glBindTexture(GL_TEXTURE_2D, t);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	free(row);
	free(pixels);

	return t;
}

static int
events()
{
	SDL_Event e;
	while (SDL_PollEvent(&e))
	{
		switch (e.type)
		{
		case SDL_QUIT:
			return 0;
			break;
		case SDL_WINDOWEVENT:
			if (e.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
			{
				win.w = e.window.data1;
				win.h = e.window.data2;
				glViewport(0, 0, win.w, win.h);
			}
			else if (e.window.event == SDL_WINDOWEVENT_SHOWN)
			{
				SDL_GetWindowSize(sdlwin, &(win.w), &(win.h));
				glViewport(0, 0, win.w, win.h);
			}
			break;
		case SDL_KEYDOWN:
			keyboard( e.key.keysym.sym);
			break;
		case SDL_KEYUP:
			break;
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
			break;
		case SDL_MOUSEMOTION:
			break;
		case SDL_MOUSEWHEEL:
			break;
		default:
			break;
		}
	}
	return 1;
}

static void
keyboard(SDL_Keycode key)
{
	switch (key) {
	case SDLK_ESCAPE:
	case SDLK_q:
		quit();
		break;
	case SDLK_g:
		state.wireframe = !state.wireframe;
		printf("Polygon mode: %s\n", state.wireframe ? "wireframe" : "fill");
		break;
	case SDLK_n:
		printf("Reloading shaders...\n");
		loadshaders(progs, shaders, LENGTH(shaders));
		loadlocs();
		break;
	case SDLK_r:
		state.revnormals = !state.revnormals;
		printf("Normals %s\n", state.revnormals ? "reversed" : "unchanged");
		break;
	case SDLK_SPACE:
		state.animating = !state.animating;
		printf("Animation %s\n", state.animating ? "started" : "stopped");
		break;
	default:
		printf("Unknown key: %u\n", (char) key);
	}
}

static void
init()
{
	if (!SDL_WasInit(sdlflags))
		if (SDL_Init(sdlflags))
			sdldie("cannot initialize SDL\n");
	atexit(SDL_Quit);

	/* https://wiki.libsdl.org/SDL_GL_SetAttribute */
	SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8 );
	//SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );
	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 8 );
	//SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES, 4 );
	SDL_GL_SetAttribute( SDL_GL_ACCELERATED_VISUAL, 1 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	sdlwin = SDL_CreateWindow(win.title, win.x, win.y, win.w, win.h,
							SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	if (!sdlwin)
		sdldie("cannot create SDL window\n");
	sdlerrcheck(__LINE__);

	glcontext = SDL_GL_CreateContext(sdlwin);
	sdlerrcheck(__LINE__);

	SDL_GL_SetSwapInterval(1);
	sdlerrcheck(__LINE__);

	glewExperimental = GL_TRUE; /* support for experimental drivers */
	GLenum rev = glewInit();
	if ( rev != GLEW_OK )
		die("GLEW Error: %s\n", glewGetErrorString(rev));
	glGetError(); /* Ignore GLEW error. cf. https://www.opengl.org/wiki/OpenGL_Loading_Library#GLEW_.28OpenGL_Extension_Wrangler.29 */
	infogl(0);

	//glEnable(GL_DEBUG_OUTPUT);
	glDisable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(debugcallback, NULL);

	glClearColor(0.2, 0.2, 0.2, 1.0);
	glEnable(GL_DEPTH_TEST);
	glPolygonMode(GL_FRONT_AND_BACK, state.wireframe ? GL_LINE : GL_FILL);

	loadshaders(progs, shaders, LENGTH(shaders));
	loadlocs();
	glUseProgram(progs[ProgBase]);
	//texture = loadtexture("fonts/zyRXC-sdf.ff", GL_TEXTURE0);

	glGenVertexArrays(N_VAO, vao);
	glGenBuffers(N_VBO, vbo);
	glBindVertexArray(vao[0]);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * lenvtx, vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(locvertex, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, v));
	glVertexAttribPointer(loctexcoord, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, vt));
	glVertexAttribPointer(locnormal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, vn));
	glEnableVertexAttribArray(locvertex);
	glEnableVertexAttribArray(loctexcoord);
	glEnableVertexAttribArray(locnormal);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * lenidx, indices, GL_STATIC_DRAW);

	glBindVertexArray(0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	checkglerr("", __LINE__);
}

static void
display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(progs[ProgBase]);
	glUniform1i(loctex, 0); /* GL_TEXTURE0 */
	glActiveTexture(GL_TEXTURE0);

	glPolygonMode(GL_FRONT_AND_BACK, state.wireframe ? GL_LINE : GL_FILL);
	glUniform1i(lochasnormals, /*state.hasnormals*/ 0);
	glUniform1i(locrevnormals, state.revnormals);

	perspective(&matrProj, 35.0, (float)win.w/(float)win.h, 0.1, 500.0);
	glUniformMatrix4fv(locmatrproj, 1, GL_FALSE, (GLfloat *)&matrProj.m);

	identity(&matrVisu);
	lookat(&matrVisu, (Vec3){0.0,2.0,1.0}, (Vec3){0.0,0.0,0.0}, (Vec3){0.0,0.0,1.0});
	glUniformMatrix4fv(locmatrvisu, 1, GL_FALSE, (GLfloat *)&matrVisu.m);

	identity(&matrModel);
	rotate(&matrModel, dtor(state.animangle), (Vec3){0.0, 0.0, 1.0});
	// rotate(&matrModel, dtor(90.0), (Vec3){1.0, 0.0, 0.0});
	rotate(&matrModel, dtor(90.0), (Vec3){1.0, 0.0, 0.0});
	Vec3 mid = addv(addv(bbox.size, bbox.orig), bbox.orig);
	float maxsize = maxcomp(bbox.size);
	scale(&matrModel, 1/maxsize, 1/maxsize, 1/maxsize);
	translate(&matrModel, (Vec3){-mid.x/2, -mid.y/2, -mid.z/2});
	glUniformMatrix4fv(locmatrmodel, 1, GL_FALSE, (GLfloat *)&matrModel.m);

	matrNorm = matrVisu;
	multiply(&matrNorm, &matrModel);
	if (!inverse(&matrNorm))
		fprintf(stderr, "ModelView matrix is not invertible\n");
	transpose(&matrNorm);
	glUniformMatrix4fv(locmatrnorm, 1, GL_FALSE, (GLfloat *)&matrNorm.m);

	glVertexAttrib3f(loccolor, 1.0, 1.0, 0.5);


	glBindVertexArray(vao[0]);
	glDrawElements(GL_TRIANGLES, lenidx, GL_UNSIGNED_INT, NULL);
	glBindVertexArray(0);

	/* axis */
	//glUseProgram(0);
	glBegin(GL_LINES);
	{
		glColor3f(1,0,0);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(2.0,0.0,0.0);
		glColor3f(0,1,0);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(0.0,2.0,0.0);
		glColor3f(0,0,1);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(0.0,0.0,2.0);
	}
	glEnd();
}

static void
animate()
{
	if (state.animating)
		state.animangle = (state.animangle < 360.0 ? state.animangle + 1.0 : 0.0);
}

static void
run()
{
	while (events()) {
		animate();
		display();
		SDL_GL_SwapWindow(sdlwin);
	}
}

static void
quit()
{
	SDL_Event ev;
	ev.type = SDL_QUIT;
	SDL_PushEvent(&ev);
	SDL_GL_DeleteContext(glcontext);
	SDL_DestroyWindow(sdlwin);
	glcontext = NULL;
	sdlwin = NULL;
	glUseProgram(0);
	glDeleteVertexArrays(N_VAO, vao);
	glDeleteBuffers(N_VBO, vbo);
	// glDeleteBuffers( 4, ubo );
}

int
main(int argc, char **argv)
{
	char *objstr;

	if (argc != 2)
		die("Usage: %s <OBJ FILE>", argv[0]);

	objstr = readfile(argv[1]);
	parseobj(objstr, &indices, &lenidx, &vertices, &lenvtx, &bbox);
	free(objstr);

	init();
	run();
	quit();
}
