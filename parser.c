#include <ctype.h>
#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "geom.h"
#include "parser.h"
#include "util.h"

int
parseobj(char *objstr, GLuint **indices, size_t *lenidx,
		Vertex **vertices, size_t *lenvtx, AABB *bbox)
{
	char *line;
	unsigned int vertnb = 0;
	unsigned int facenb = 0;
	unsigned int idxnb = 0;
	unsigned int overflownb = 0;
	unsigned int normnb = 0;
	unsigned int texcnb = 0;
	unsigned int ln = 0; // Line number


	line = objstr;
	while (line) {
		char * next = strchr(line, '\n');
		if (next)
			*next = '\0'; // Temp line termination
		// TODO : Process line
		if (line[0] == 'v') {
			if (isspace(line[1]))
				vertnb++;
			else if (line[1] == 't')
				texcnb++;
			else if (line[1] == 'n')
				normnb++;
			else
				fprintf(stderr, "Unknown directive: v%c\n", line[1]);
		} else if (line[0] == 'f' && isspace(line[1])) {
			facenb++;
			// Count the number of vertices in this face
			char last = ' ';
			unsigned int vcount = 0;
			// Index to select v, vt, or vn
			int idx = 0;
			enum {VTX, TEX, NOR};
			int vertidx[3] = {0, NO_VALUE, NO_VALUE};
			for (char * p = line + 2; *p != '\0'; p++) {
				// We do a bit of parsing to determine how many overflow vertices we have
				if (!isspace(*p) && (isspace(last) || last == '/')) {
					int i = 1;
					sscanf(p, "%d", &i);
					if (i < 0) { // TODO : Add support
						fprintf(stderr, "l.%d: Relative indices are not supported\n  %s\n", ln, line);
						break;
					}
					vertidx[idx] = i - 1; // Indices begin at 1 #Matlab
				}
				if (*p == '/') {
					idx++;
				} else if (!isspace(*p) && (isspace(*(p+1)) || *(p+1) == '\0')) {
					if (!(vertidx[VTX] == vertidx[TEX] || vertidx[TEX] == NO_VALUE) ||
							!(vertidx[VTX] == vertidx[NOR] || vertidx[NOR] == NO_VALUE)) {
						overflownb++;
					}
					idx = 0;
					vcount++;
				}
				if (idx > 2) {
					fprintf(stderr, "l.%d: Illegal index declaration:\n  %s\n", ln, line);
					break;
				}
				last = *p;
			}
			// We form the polygons using GL_TRIANGLES
			idxnb += (vcount - 2) * 3;
		} else if (line[0] == '\0') {
			// TODO : remove debug and just skip
			// fprintf(stderr, "Line %d is empty\n", ln);
		} else if (line[0] == '#') {
			// TODO : remove debug and just skip
		} else {
			// FIXME All kinds of security issues here
			//fprintf(stderr, "Cannot parse line %d:\n\"%s\"\n", ln, line);
		}
		if (next)
			*next = '\n';
		line = next ? (next + 1) : NULL;
		ln++;
	}

	printf("We found %d vertices, %d normals, %d tc, %d faces, %d indices, and %d overflow vertices.\n",
			vertnb, normnb, texcnb, facenb, idxnb, overflownb);

	*lenidx = idxnb; // TODO : simplify, maybe ?
	if (!(*indices = malloc(*lenidx * sizeof(GLuint))))
		die("Out of memory for the indices.\n");

	*lenvtx = MAX(MAX(vertnb, texcnb), normnb) + overflownb;
	if (!(*vertices = malloc(*lenvtx * sizeof(Vertex))))
		die("Out of memory for the vertices.\n");

	//if (normnb > 0)
	//	state.hasnormals = 1;

	// Indices for the different elements. We could have repurposed the ...nb ones
	size_t vi = 0;
	size_t vti = 0;
	size_t vni = 0;
	size_t fi = 0;
	size_t overflowidx = *lenvtx - overflownb; // Index for storing mismatched face attributes
	Vec3 bboxmin = {FLT_MAX, FLT_MAX, FLT_MAX};
	Vec3 bboxmax = {-FLT_MAX, -FLT_MAX, -FLT_MAX};
	int code; // sscanf return code
	ln = 0; // reset line number
	line = objstr;
	while (line) {
		char * next = strchr(line, '\n');
		if (next)
			*next = '\0'; // Temp mine termination
		// TODO : Process line
		// printf("Processing l.%d:\n  %s\n", ln, line);
		if (line[0] == 'v') {
			if (isspace(line[1])) {
				// Process vertex
				Vertex *v = *vertices + vi++;
				// Assign default values in case parsing fails
				v->v.x = v->v.y = v->v.z = 0.0;
				// Only w can be omitted
				v->v.w = 1.0;
				if (sscanf(line, "v %f %f %f %f", &v->v.x, &v->v.y, &v->v.z, &v->v.w) < 3)
					fprintf(stderr, "l.%d: malformed vertex coordinates\n", ln);
				bboxmin = minv(bboxmin, vec4to3(v->v));
				bboxmax = maxv(bboxmax, vec4to3(v->v));
			} else if (line[1] == 't') {
				// Process texture coords
				Vertex *v = *vertices + vti++;
				v->vt.x = v->vt.y = v->vt.z = 0.0; // v and s are optional
				if (sscanf(line, "vt %f %f %f", &v->vt.x, &v->vt.y, &v->vt.z) < 1)
					fprintf(stderr, "l.%d: malformed texture coordinates\n", ln);
			} else if (line[1] == 'n') {
				// Process normals
				Vertex *v = *vertices + vni++;
				v->vn.x = v->vn.y = v->vn.z = 0.0;
				if (sscanf(line, "vn %f %f %f", &v->vn.x, &v->vn.y, &v->vn.z) < 3)
					fprintf(stderr, "l.%d: malformed normal coordinates\n", ln);
			} else {
				fprintf(stderr, "Unknown directive: v%c\n", line[1]);
			}
		} else if (line[0] == 'f' && isspace(line[1])) {
			// Add indices for the face
			char last = ' ';
			// Index to select v, vt, or vn
			int idx = 0;
			int vcount = 0;
			enum {VTX, TEX, NOR};
			int vertidx[3] = {0, NO_VALUE, NO_VALUE};
			// TODO Deal with illegal index states
			// TODO Deal with different tc and n indices
			for (char * p = line + 2; *p != '\0'; p++) {
				if (!isspace(*p) && (isspace(last) || last == '/')) {
					int i = 1;
					sscanf(p, "%d", &i);
					if (i < 0) { // TODO : Add support
						fprintf(stderr, "l.%d: Relative indices are not supported\n  %s\n", ln, line);
						break;
					}
					vertidx[idx] = i - 1; // Indices begin at 1 #Matlab
				}
				if (*p == '/') {
					idx++;
				} else if (!isspace(*p) && (isspace(*(p+1)) || *(p+1) == '\0')) {
					if ((vertidx[VTX] == vertidx[TEX] || vertidx[TEX] == NO_VALUE) &&
							(vertidx[VTX] == vertidx[NOR] || vertidx[NOR] == NO_VALUE)) {
						(*indices)[fi + vcount] = vertidx[VTX];
					} else {
						Vertex *v = *vertices + overflowidx;
						v->v = (*vertices)[vertidx[VTX]].v;
						v->vt = (*vertices)[vertidx[TEX] != NO_VALUE ? vertidx[TEX] : vertidx[VTX]].vt;
						v->vn = (*vertices)[vertidx[TEX] != NO_VALUE ? vertidx[NOR] : vertidx[VTX]].vn;
						(*indices)[fi + vcount] = overflowidx++;
					}
					idx = 0;
					vcount++;
					if (vcount > 3) { // We have more than 1 triangle
						(*indices)[fi + vcount++] = (*indices)[fi];
						/* The first triangle is not wound like the others,
						 * that explains the dirty ternary.
						 * 5 in the inequality is the index of Vertex 2 for the second triangle*/
						(*indices)[fi + vcount] = (*indices)[fi + vcount - (vcount > 5 ? 5 : 3)];
						vcount++;
					}
				}
				if (idx > 2) {
					fprintf(stderr, "l.%d: Illegal index declaration:\n  %s\n", ln, line);
					break;
				}
				last = *p;
			}
			fi += vcount;
		} else if (line[0] == 'o') {
			// TODO Do something useful with this name
			// FIXME Doesn't handle additional whitespace
			printf("Object name: %s\n", line + 2);
		} else if (line[0] == 's') { // TODO smoothing groups
			// Smoothing groups seem to be more or less redondant with normals
			// We'll see if we encounter a file with smoothing groups and no vn
			fprintf(stderr, "l.%d: smoothing groups are not yet supported.\n", ln);
		} else if (!strncmp("mtllib", line, strlen("mtllib"))) { // TODO mtllib
			fprintf(stderr, "l.%d: 'mtllib' not implemented.\n", ln);
		} else if (!strncmp("usemtl", line, strlen("usemtl"))) { // TODO usemtl
			fprintf(stderr, "l.%d: 'usemtl' not implemented.\n", ln);
		} else if (line[0] == '\0') {
			// TODO : remove debug and just skip
		} else if (line[0] == '#') {
			// TODO : remove debug and just skip
		} else if (isspace(line[0])) {
			// TODO : remove debug and just skip
		} else {
			// FIXME All kinds of security issues here
			fprintf(stderr, "Cannot parse line %d:\n\"%s\"\n", ln, line);
			printhexstring(line);
		}
		if (next)
			*next = '\n';
		line = next ? (next + 1) : NULL;
		ln++;
	}

	bbox->orig = bboxmin;
	bbox->size = subv(bboxmax, bboxmin);
	return 0; // FIXME real rturn values
}
