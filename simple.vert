#version 410

uniform mat4 matrModel;
uniform mat4 matrVisu;
uniform mat4 matrProj;
uniform mat4 matrNorm;

layout(location=0) in vec4 v;
layout(location=3) in vec3 vt;
layout(location=8) in vec3 vn;

out Attr {
    vec3 vt;
    vec3 vn;
    vec3 L;
    vec3 O;
} o;

const vec4 lightworld = vec4(vec3(1000), 1);

void main( void )
{
    gl_Position = matrVisu * matrModel * v;
    o.vt = vt;
    o.vn = mat3(matrNorm) * vn;
    vec3 pos = gl_Position.xyz;
    vec3 lightpos = (matrVisu * lightworld).xyz;
    o.O = -pos;
    o.L = lightpos - pos;
}

