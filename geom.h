#define PI 3.14159265358979323846
#define MAX_STACK_SIZE 1024

/* Matrix stored in column-major form */
typedef struct {
	float m[16];
} Mat4;

typedef struct {
	float x;
	float y;
	float z;
} Vec3;

typedef struct {
	float x;
	float y;
	float z;
	float w;
} Vec4;

typedef struct {
	Vec3 orig;
	Vec3 size;
} AABB;

typedef struct {
	Mat4 *contents;
	int top;
} Matstk;

typedef struct {
	Vec4 v;
	Vec3 vt;
	Vec3 vn;
} Vertex;


float dtor(float deg);
void multiply(Mat4 *a, Mat4 *b);
void rotate(Mat4 *m,float rad, Vec3 axis);
void identity(Mat4 *a);
void translate(Mat4 *m, Vec3 v);
void scale(Mat4 *m, float x, float y, float z);
void perspective(Mat4 *m, float fovy, float aspect, float near, float far);
void lookat(Mat4 *a, Vec3 eye, Vec3 pos, Vec3 up);
int inverse(Mat4 *m);
void transpose(Mat4 *m);
Vec3 normalize(Vec3 v);
Vec3 cross(Vec3 u, Vec3 v);
Vec3 maxv(Vec3 a, Vec3 b);
float maxcomp(Vec3 v);
Vec3 minv(Vec3 a, Vec3 b);
Vec3 subv(Vec3 a, Vec3 b);
Vec3 addv(Vec3 a, Vec3 b);
Vec3 vec4to3(Vec4 v);
void stackinit(Matstk *s);
void stackdestroy(Matstk *s);
void stackpush(Matstk *s, Mat4 mat);
Mat4 stackpop(Matstk *s);
void printmat(const Mat4 m, const char *name);
void printvec(const Vec3 v, const char *name);
