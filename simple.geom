#version 410

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

uniform mat4 matrProj;

uniform bool hasnormals;
uniform bool revnormals;

in Attr {
    vec3 vt;
    vec3 vn;
    vec3 L;
    vec3 O;
} ingress[];

out Attr {
    vec3 vt;
    vec3 vn;
    vec3 L;
    vec3 O;
} egress;

void main()
{
    vec3 u = gl_in[0].gl_Position.xyz - gl_in[1].gl_Position.xyz;
    vec3 v = gl_in[0].gl_Position.xyz - gl_in[2].gl_Position.xyz;
    vec3 n = cross(u, v);

    for (int i = 0; i < gl_in.length(); i++) {
        gl_Position = matrProj *  gl_in[i].gl_Position;
        egress.vt = ingress[i].vt;
        egress.vn = hasnormals ? ingress[i].vn : n;
        if (revnormals)
            egress.vn *= -1;
        egress.L = ingress[i].L;
        egress.O = ingress[i].O;
        EmitVertex();
    }
    EndPrimitive();
}
