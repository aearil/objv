DIR_INC		=	.
DIR_SRC		=	.
DIR_OBJ		=	./obj
DIR_BIN		=	./bin

CC			=	gcc
CFLAGS		=	-g -std=c99 -pedantic -Wall -I$(DIR_INC)
LDFLAGS		=	-lSDL2 -lGL -lGLEW -lm
LIBS		=

SRC			=	$(wildcard $(DIR_SRC)/*.c)
OBJ			=	$(subst src/,obj/,$(SRC:.c=.o))
INC_H		:=	$(shell find $(DIR_INC) -name '*.h')
INC_CPP		:=	#$(shell find $(DIR_INC) -name '*.c')

TARGET		=	objv

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) $^ $(LDFLAGS) $(INCLUDES) -o $@

$(DIR_OBJ)/%.o: $(DIR_SRC)/%.c $(INC_H) $(INC_CPP)
	$(CC) -c $^ $(CFLAGS) -o $@

run: $(TARGET)
	./$(TARGET) $(ARGS)

.PHONY:clean
clean:
	-rm -f *.o
	-rm -f $(TARGET)
