#version 410

uniform sampler2D tex;

in Attr {
    vec3 vt;
    vec3 vn;
    vec3 L;
    vec3 O;
} ingress;

out vec4 FragColor;

const vec4 Ke = vec4(0, 0, 0, 1);
const vec4 Ka = vec4(.3, .3, .3, 1);
const vec4 Kd = vec4(.7, .6, .6, 1);
const vec4 Ks = vec4(1);
const vec4 Ia = vec4(1);
const vec4 Id = vec4(1);
const vec4 Is = vec4(1);

void main( void )
{
    vec3 L = normalize(ingress.L);
    vec3 O = normalize(ingress.O);
    vec3 N = normalize(ingress.vn);
    vec3 B = normalize(L + O);
    vec4 I = Ke + Ia * Ka;
    float NdL = dot(N, L);
    if (NdL > 0) {
        I += Id * Kd * dot(N, L);
        I += Is * Ks * pow(dot(B, N), 100);
    }
    FragColor = I;
}

