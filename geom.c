#include <GL/glew.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "geom.h"
#include "util.h"

/* Needed matrix functions :
 * LoadIdentity
 * Scale(x, y, z)
 * Translate(x, y, z)
 * Rotate(angle, x, y, z)
 * LookAt(obsx, obsy, obsz, objx, objy, objz, upx, upy, upz)
 * Frustum ?
 * Perspective(fovy, ratio, near, far)
 * Ortho(left, right, down, up, near, far)
 * Ortho2D(left, right, down, up)
 * PushMatrix
 * PopMatrix
 * toString
 * to const GLfloat
 **/


static const Mat4 IDENTITY = {{
	1,0,0,0,
	0,1,0,0,
	0,0,1,0,
	0,0,0,1
}};


float
dtor(float deg)
{
	return deg * PI / 180.0f;
}

void
multiply(Mat4 *a, Mat4 *b)
{
	Mat4 c;
	int i, j;
	for (i = 0; i < 4; ++i) {
		for (j = 0; j < 4; ++j) {
			c.m[4*j+i] =
				(a->m[i+0]  * b->m[4*j+0]) +
				(a->m[i+4]  * b->m[4*j+1]) +
				(a->m[i+8]  * b->m[4*j+2]) +
				(a->m[i+12] * b->m[4*j+3]);
		}
	}
	for (i = 0; i < 16; i++)
		a->m[i] = c.m[i];
}

void
rotate(Mat4 *m, float rad, Vec3 axis)
{
	float x, y, z;
	float c = cos(rad);
	float s = sin(rad);
	axis = normalize(axis);
	x = axis.x;
	y = axis.y;
	z = axis.z;
	Mat4 r = {{
		(1-c)*x*x + c,   (1-c)*x*y + s*z, (1-c)*x*z - s*y, 0.,
		(1-c)*x*y - s*z, (1-c)*y*y + c,   (1-c)*y*z + s*x, 0.,
		(1-c)*x*z + s*y, (1-c)*y*z - s*x, (1-c)*z*z + c,   0.,
		0.,              0.,              0.,              1.
	}}; /* Transposed matrix because OpenGL */

	multiply(m, &r);
}

void
identity(Mat4 *a)
{
	float *m = a->m;
	m[0] = m[5] = m[10] = m[15] = 1.0f;
	m[1] = m[2] = m[3] = m[4] = m[6] = m[7] = m[8] = m[9] = m[11] = m[12] = m[13] = m[14] = 0.0f;
}

void
transpose(Mat4 *m)
{
	for (int i = 0; i < 4; i++) {
		for (int j = 3; j > i; j--) {
			float tmp = m->m[i*4+j];
			m->m[i*4+j] = m->m[j*4+i];
			m->m[j*4+i] = tmp;
		}
	}
}

void
translate(Mat4 *m, Vec3 v)
{
	Mat4 t = IDENTITY;
	t.m[12] = v.x;
	t.m[13] = v.y;
	t.m[14] = v.z;
	multiply(m, &t);
}

void
scale(Mat4 *m, float x, float y, float z)
{
	Mat4 s = IDENTITY;
	s.m[0] = x;
	s.m[5] = y;
	s.m[10] = z;
	multiply(m, &s);
}

void
perspective(Mat4 *m, float fovy, float aspect, float near, float far)
{
	float f = 1.0f / tan(dtor(fovy / 2.0f));
	m->m[1] = m->m[2] = m->m[3] = 0.0f;
	m->m[4] = m->m[6] = m->m[7] = 0.0f;
	m->m[8] = m->m[9] = 0.0f;
	m->m[12] = m->m[13] = m->m[15] = 0.0f;
	m->m[0] = f / aspect;
	m->m[5] = f;
	m->m[10] = (far + near) / (near - far);
	m->m[11] = -1;
	m->m[14] = (2 * near * far) / (near - far);
}

void
lookat(Mat4 *a, Vec3 eye, Vec3 pos, Vec3 up)
{
	Vec3 f, s, u;
	Mat4 m;

	f.x = pos.x - eye.x;
	f.y = pos.y - eye.y;
	f.z = pos.z - eye.z;
	f = normalize(f);
	up = normalize(up);
	s = cross(f, up);
	u = cross(normalize(s), f);

	m.m[3] = m.m[7] = m.m[11] = m.m[12] = m.m[13] = m.m[14] = 0.0f;
	m.m[0] = s.x;
	m.m[1] = u.x;
	m.m[2] = -f.x;
	m.m[4] = s.y;
	m.m[5] = u.y;
	m.m[6] = -f.y;
	m.m[8] = s.z;
	m.m[9] = u.z;
	m.m[10] = -f.z;
	m.m[15] = 1.0f;


	multiply(a, &m);
	translate(a, (Vec3){-eye.x, -eye.y, -eye.z});
}

int
inverse(Mat4 *m)
{
    double inv[16], det;
    int i;

    inv[0] = m->m[5]  * m->m[10] * m->m[15] -
             m->m[5]  * m->m[11] * m->m[14] -
             m->m[9]  * m->m[6]  * m->m[15] +
             m->m[9]  * m->m[7]  * m->m[14] +
             m->m[13] * m->m[6]  * m->m[11] -
             m->m[13] * m->m[7]  * m->m[10];

    inv[4] = -m->m[4]  * m->m[10] * m->m[15] +
              m->m[4]  * m->m[11] * m->m[14] +
              m->m[8]  * m->m[6]  * m->m[15] -
              m->m[8]  * m->m[7]  * m->m[14] -
              m->m[12] * m->m[6]  * m->m[11] +
              m->m[12] * m->m[7]  * m->m[10];

    inv[8] = m->m[4]  * m->m[9] * m->m[15] -
             m->m[4]  * m->m[11] * m->m[13] -
             m->m[8]  * m->m[5] * m->m[15] +
             m->m[8]  * m->m[7] * m->m[13] +
             m->m[12] * m->m[5] * m->m[11] -
             m->m[12] * m->m[7] * m->m[9];

    inv[12] = -m->m[4]  * m->m[9] * m->m[14] +
               m->m[4]  * m->m[10] * m->m[13] +
               m->m[8]  * m->m[5] * m->m[14] -
               m->m[8]  * m->m[6] * m->m[13] -
               m->m[12] * m->m[5] * m->m[10] +
               m->m[12] * m->m[6] * m->m[9];

    inv[1] = -m->m[1]  * m->m[10] * m->m[15] +
              m->m[1]  * m->m[11] * m->m[14] +
              m->m[9]  * m->m[2] * m->m[15] -
              m->m[9]  * m->m[3] * m->m[14] -
              m->m[13] * m->m[2] * m->m[11] +
              m->m[13] * m->m[3] * m->m[10];

    inv[5] = m->m[0]  * m->m[10] * m->m[15] -
             m->m[0]  * m->m[11] * m->m[14] -
             m->m[8]  * m->m[2] * m->m[15] +
             m->m[8]  * m->m[3] * m->m[14] +
             m->m[12] * m->m[2] * m->m[11] -
             m->m[12] * m->m[3] * m->m[10];

    inv[9] = -m->m[0]  * m->m[9] * m->m[15] +
              m->m[0]  * m->m[11] * m->m[13] +
              m->m[8]  * m->m[1] * m->m[15] -
              m->m[8]  * m->m[3] * m->m[13] -
              m->m[12] * m->m[1] * m->m[11] +
              m->m[12] * m->m[3] * m->m[9];

    inv[13] = m->m[0]  * m->m[9] * m->m[14] -
              m->m[0]  * m->m[10] * m->m[13] -
              m->m[8]  * m->m[1] * m->m[14] +
              m->m[8]  * m->m[2] * m->m[13] +
              m->m[12] * m->m[1] * m->m[10] -
              m->m[12] * m->m[2] * m->m[9];

    inv[2] = m->m[1]  * m->m[6] * m->m[15] -
             m->m[1]  * m->m[7] * m->m[14] -
             m->m[5]  * m->m[2] * m->m[15] +
             m->m[5]  * m->m[3] * m->m[14] +
             m->m[13] * m->m[2] * m->m[7] -
             m->m[13] * m->m[3] * m->m[6];

    inv[6] = -m->m[0]  * m->m[6] * m->m[15] +
              m->m[0]  * m->m[7] * m->m[14] +
              m->m[4]  * m->m[2] * m->m[15] -
              m->m[4]  * m->m[3] * m->m[14] -
              m->m[12] * m->m[2] * m->m[7] +
              m->m[12] * m->m[3] * m->m[6];

    inv[10] = m->m[0]  * m->m[5] * m->m[15] -
              m->m[0]  * m->m[7] * m->m[13] -
              m->m[4]  * m->m[1] * m->m[15] +
              m->m[4]  * m->m[3] * m->m[13] +
              m->m[12] * m->m[1] * m->m[7] -
              m->m[12] * m->m[3] * m->m[5];

    inv[14] = -m->m[0]  * m->m[5] * m->m[14] +
               m->m[0]  * m->m[6] * m->m[13] +
               m->m[4]  * m->m[1] * m->m[14] -
               m->m[4]  * m->m[2] * m->m[13] -
               m->m[12] * m->m[1] * m->m[6] +
               m->m[12] * m->m[2] * m->m[5];

    inv[3] = -m->m[1] * m->m[6] * m->m[11] +
              m->m[1] * m->m[7] * m->m[10] +
              m->m[5] * m->m[2] * m->m[11] -
              m->m[5] * m->m[3] * m->m[10] -
              m->m[9] * m->m[2] * m->m[7] +
              m->m[9] * m->m[3] * m->m[6];

    inv[7] = m->m[0] * m->m[6] * m->m[11] -
             m->m[0] * m->m[7] * m->m[10] -
             m->m[4] * m->m[2] * m->m[11] +
             m->m[4] * m->m[3] * m->m[10] +
             m->m[8] * m->m[2] * m->m[7] -
             m->m[8] * m->m[3] * m->m[6];

    inv[11] = -m->m[0] * m->m[5] * m->m[11] +
               m->m[0] * m->m[7] * m->m[9] +
               m->m[4] * m->m[1] * m->m[11] -
               m->m[4] * m->m[3] * m->m[9] -
               m->m[8] * m->m[1] * m->m[7] +
               m->m[8] * m->m[3] * m->m[5];

    inv[15] = m->m[0] * m->m[5] * m->m[10] -
              m->m[0] * m->m[6] * m->m[9] -
              m->m[4] * m->m[1] * m->m[10] +
              m->m[4] * m->m[2] * m->m[9] +
              m->m[8] * m->m[1] * m->m[6] -
              m->m[8] * m->m[2] * m->m[5];

    det = m->m[0] * inv[0] + m->m[1] * inv[4] + m->m[2] * inv[8] + m->m[3] * inv[12];

    if (det == 0)
        return 0;

    det = 1.0 / det;

    for (i = 0; i < 16; i++)
        m->m[i] = inv[i] * det;

    return 1;
}

Vec3
normalize(Vec3 v)
{
	float l = sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
	v.x /= l;
	v.y /= l;
	v.z /= l;
	return v;
}

Vec3
cross(Vec3 u, Vec3 v)
{
	Vec3 w;
	w.x = u.y * v.z - u.z * v.y;
	w.y = u.z * v.x - u.x * v.z;
	w.z = u.x * v.y - u.y * v.x;
	return w;
}

Vec3
maxv(Vec3 a, Vec3 b)
{
	Vec3 r;
	r.x = MAX(a.x, b.x);
	r.y = MAX(a.y, b.y);
	r.z = MAX(a.z, b.z);
	return r;
}

float
maxcomp(Vec3 v)
{
	return MAX(MAX(v.x, v.y), v.z);
}

Vec3
minv(Vec3 a, Vec3 b)
{
	Vec3 r;
	r.x = MIN(a.x, b.x);
	r.y = MIN(a.y, b.y);
	r.z = MIN(a.z, b.z);
	return r;
}

Vec3
subv(Vec3 a, Vec3 b)
{
	Vec3 r;
	r.x = a.x - b.x;
	r.y = a.y - b.y;
	r.z = a.z - b.z;
	return r;
}

Vec3
addv(Vec3 a, Vec3 b)
{
	Vec3 r;
	r.x = a.x + b.x;
	r.y = a.y + b.y;
	r.z = a.z + b.z;
	return r;
}

Vec3
vec4to3(Vec4 v)
{
	return (Vec3){v.x, v.y, v.z};
}

void
stackinit(Matstk *s)
{
	Mat4 *contents;
	contents = (Mat4 *)malloc(sizeof(Mat4) * MAX_STACK_SIZE);

	if (contents == NULL)
		die("malloc: initstack");

	s->contents = contents;
	s->top = -1; /* empty */
}

void
stackdestroy(Matstk *s)
{
	free(s->contents);
	s->contents = NULL;
	s->top = -1;
}

void
stackpush(Matstk *s, Mat4 mat)
{
	if (s->top >= MAX_STACK_SIZE - 1)
		die("matrix stack is full");
	s->contents[++s->top] = mat;
}

Mat4
stackpop(Matstk *s)
{
	if (s->top < 0)
		die("matrix stack is empty");
	return s->contents[s->top--];
}

void
printmat(const Mat4 m, const char *name)
{
	int i;
	printf("Matrix %s\n", name);
	for (i = 0; i < 4; ++i)
		printf("| %f | %f | %f | %f |\n", m.m[i], m.m[i+4], m.m[i+8], m.m[i+12]);
}

void
printvec(const Vec3 v, const char *name)
{
	printf("%s : | %f | %f | %f |\n",name, v.x, v.y, v.z);
}
